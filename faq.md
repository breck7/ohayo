#### What is ohayo?

ohayo is the data dashboard maker.

#### Where are my boards saved?

In this early release, just to local storage. More persistence options coming soon.

#### Is ohayo open source?

Just about. You can get the Tree Notation source here: https://github.com/breck7/jtree/ and the minified
version of Ohayo here https://github.com/breck7/ohayo . With the latter you can run and modify Ohayo
locally or on your own servers. The full source is coming soon, pending a few more refactors.

#### How can I support Ohayo?

Thanks for asking! Providing your feedback is very helpful! Send feedback to feedback@ohayo.computer